#!/usr/bin/python
#-*-coding: utf-8 -*-

from Model import Model

class AskMe:
	def __init__(self):
		self.model = Model()

	def menu(self):
		print "================================="
		print "  Enter 't' for training mode"
		print "  Enter 'a' for asking mode"
		print "  Enter 'i' for import model"
		print "  Enter 'e' for export model"
		print "  Enter 'x' for exit"
		print "================================="
		cmd = raw_input("Enter command : ")
		if cmd == "t":
			print "================================="
			print "  Enter 'exit' for stop training "
			print "================================="
			while(True):
				question = raw_input("enter Q: ").strip()
				if question == "exit":
					self.learn()
					break
				answer = raw_input("enter A: ").strip()
				self.train(question,answer)
				print "\n...loading...\n\n"


		elif cmd == "a":
			question = raw_input("enter Q: ")
			print "Answer: ",self.ask(question)

		elif cmd == "i":
			filename = raw_input("enter filename: ")
			self.importModel(filename)

		elif cmd == "e":
			filename = raw_input("enter filename: ")
			self.exportModel(filename)

		elif cmd == "x":
			print "Bye"
			return False
		else:
			print "Don't fucking doing like this!"

		return True

	def learn(self):
		self.model.learn()

	def train(self,question,answer):
		try:
			self.model.train(question,answer)
		except Exception as e:
			print "Error : ",e

	def ask(self,question):
		return self.model.ask(question)

	def listen(self,port):
		return port

	def importModel(self,filename):
		pass

	def exportModel(self,filename):
		pass

	def log(self):
		pass

if __name__ == "__main__":

    askme = AskMe()
    while True:
    	if not askme.menu():
    		break
    

