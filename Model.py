#!/usr/bin/python
#-*-coding: utf-8 -*-

from QA import QA
from kucut import SimpleKucutWrapper as kucut

class Model:
	def __init__(self):
		self.qa = [] 
		self.df = []
		self.index = {} 
		self.dictionary = [] 
		self.tokenizer = kucut()

	def preprocess(self,word):
		#remove !@#$%^&*()
		return word.strip()

	def tokenize(self,sentence):
		result = self.tokenizer.tokenize([sentence])
		return result[0][0]

	def prepare(self,question,answer,update = True):
		tokens = self.tokenize(question)
		tf = {}
		for token in tokens:
			token = self.preprocess(token)

			if token not in self.index:
				self.dictionary.append(token)
				self.df.append(0)
				self.index[token] = len(self.dictionary)-1

			if self.index[token] in tf:
				tf[self.index[token]]+=1
			else:
				tf[self.index[token]] = 1

		if update:
			for token in set(tokens):
				self.df[self.index[token]] +=1
			print self.dictionary
			print self.df

		answer = self.preprocess(answer)
		newQA = QA(question,answer)
		newQA.setup(tokens,tf)
		return newQA

	def train(self,question,answer):
		newQA = self.prepare(question,answer)
		self.qa.append(newQA)
	
	def learn(self):
		N = len(self.qa)
		for qa in self.qa:
			qa.calculate(self.df,N)

	def ask(self,question):
		askQA = self.prepare(question,"",update=False)
		askQA.calculate(self.df, len(self.qa))

		similarity = []
		for qa in self.qa:
			similarity.append(qa.sim(askQA))

		def comparetor(x,y):
			diff = (similarity[y]-similarity[x])

			if diff > 0:
				return 1
			elif diff < 0:
				return -1
			else:
				return 0

		index = range(len(similarity))
		index.sort(comparetor)
		return self.qa[index[0]].answer+" with similarity "+ str(similarity[index[0]])

