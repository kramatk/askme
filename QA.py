#!/usr/bin/python
#-*-coding: utf-8 -*-
import math

class QA:
	def __init__(self,question,answer):
		self.question = question
		self.answer = answer
		self.tokens = []
		self.tf = {}
		self.weight = {} 
		self.length = 0  #for normalize

	def setup(self,tokens,tf):
		self.tokens = tokens
		self.tf = tf

	def calculate(self,df,N):
		self.updateWight(df,N)
		self.updateLength()

	def updateWight(self,df,N):
		for index in self.tf:
			idf = 1
			if df[index]!=0:
				idf = math.log( N*1.0 / df[index])
			self.weight[index] = self.tf[index]*idf

	def updateLength(self):
		sq = 0
		for index in self.weight:
			sq += self.weight[index]*self.weight[index]*1.0

		self.length = math.sqrt(sq)

	def sim(self,qa):
		simility = 0
		for index in self.weight:
			if index in qa.weight:
				try:
					simility += (self.weight[index]/self.length)*(qa.weight[index]/qa.length)
				except Exception as e:
					simility += 0 
		return simility